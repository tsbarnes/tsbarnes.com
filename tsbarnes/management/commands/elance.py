from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from optparse import make_option
import urllib, urllib2, json

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--dry-run',
            action='store_true',
            dest='dry_run',
            default=False,
            help='Perform a test run without modifying the database'),
        )

    def handle(self, *args, **options):
        tparams = urllib.urlencode({
            'client_id': settings.ELANCE_API_KEY,
            'client_secret': settings.ELANCE_API_SECRET,
            'grant_type': 'client_credentials',
        })
        elance_post = urllib2.urlopen('https://api.elance.com/api2/oauth/token', tparams)
        tresponse = json.loads(elance_post.read())
        uparams = urllib.urlencode({
            'access_token': tresponse.data['access_token']
        })
        elance_post2 = urllib2.urlopen('https://api.elance.com/api2/contractors/' + settings.ELANCE_USER_ID, uparams)
        uresponse = json.loads(elance_post2.read())
        print uresponse
        if options['dry_run']:
            pass
