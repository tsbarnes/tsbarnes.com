from django.template import RequestContext
from django.shortcuts import render_to_response
from portfolio.models import PortfolioEntry
from blog.models import BlogEntry

def home(request):
    context_data = dict(
        portfolio=PortfolioEntry.objects.order_by('date_added').all()[:10],
        blog=BlogEntry.objects.order_by('posted').all()[:10],
    )
    return render_to_response('index.html', context_data, context_instance=RequestContext(request))

def about(request):
    return render_to_response('about.html', {}, context_instance=RequestContext(request))
