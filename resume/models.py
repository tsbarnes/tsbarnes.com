from django.db import models
from django.contrib.auth import models as auth
from django_localflavor_us import models as localflavor

from south.modelsinspector import add_introspection_rules
add_introspection_rules([], ["^django_localflavor_us\.models\.USStateField"])
add_introspection_rules([], ["^django_localflavor_us\.models\.PhoneNumberField"])

class EmailAddress(models.Model):
    class Meta:
        verbose_name_plural = 'Email addresses'
    email = models.EmailField(max_length=254)
    user = models.ForeignKey(auth.User, blank=True, null=True)

class PhoneNumber(models.Model):
    phone = localflavor.PhoneNumberField()
    user = models.ForeignKey(auth.User, null=True)

class Address(models.Model):
    class Meta:
        verbose_name_plural = 'Addresses'
    address = models.TextField(max_length=254, blank=True)
    city = models.CharField(max_length=64, blank=True)
    state = localflavor.USStateField(blank=True)
    zip_code = models.CharField(max_length=10, blank=True)
    user = models.ForeignKey(auth.User, blank=True, null=True)

class About(models.Model):
    class Meta:
        verbose_name_plural = 'About'
    summary = models.TextField(max_length=254)
    content = models.TextField(max_length=4096)
    user = models.ForeignKey(auth.User, blank=True, null=True)

class Skill(models.Model):
    name = models.CharField(max_length=48)
    description = models.TextField(max_length=254)
    user = models.ForeignKey(auth.User, blank=True, null=True)

EXPERIENCE_TYPES = (
    (0, 'Job'),
    (1, 'Volunteer'),
    (2, 'Project')
)

class Experience(models.Model):
    class Meta:
        verbose_name_plural = 'Experience'
    type = models.SmallIntegerField(choices=EXPERIENCE_TYPES)
    title = models.CharField(max_length=128)
    company = models.CharField(max_length=128, blank=True)
    started = models.DateField(blank=True, null=True)
    ended = models.DateField(blank=True, null=True)
    description = models.TextField(max_length=2048, blank=True)
    user = models.ForeignKey(auth.User, blank=True, null=True)

class Education(models.Model):
    class Meta:
        verbose_name_plural = 'Education'
    school = models.CharField(max_length=254)
    degree = models.CharField(max_length=254)
    in_progress = models.BooleanField(default=False)
    graduated = models.DateField(blank=True, null=True)
    user = models.ForeignKey(auth.User, blank=True, null=True)

class Course(models.Model):
    course = models.CharField(max_length=10)
    description = models.CharField(max_length=254)
    user = models.ForeignKey(auth.User, blank=True, null=True)

class Recommendation(models.Model):
    author = models.CharField(max_length=254)
    quote = models.TextField(max_length=4096)
    #connection = models.ForeignKey()
    user = models.ForeignKey(auth.User, blank=True, null=True)
