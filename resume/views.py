from django.template import RequestContext
from django.shortcuts import render_to_response
from . import models

def resume(request):
    emails = models.EmailAddress.objects.all()
    phones = models.PhoneNumber.objects.all()
    addresses = models.Address.objects.all()
    about = models.About.objects.all()
    skills = models.Skill.objects.all()
    jobs = models.Experience.objects.filter(type=0)
    volunteer = models.Experience.objects.filter(type=1)
    projects = models.Experience.objects.filter(type=2)
    return render_to_response('resume.html', {
        'contacts':     emails,
        'phones':       phones,
        'addresses':    addresses,
        'about':        about,
        'skills':       skills,
        'jobs':         jobs,
        'volunteer':    volunteer,
        'projects':     projects,
    }, context_instance=RequestContext(request))
