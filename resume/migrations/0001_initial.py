# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Contact'
        db.create_table(u'resume_contact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=254, blank=True)),
            ('phone', self.gf('django_localflavor_us.models.PhoneNumberField')(max_length=20, blank=True)),
        ))
        db.send_create_signal(u'resume', ['Contact'])

        # Adding model 'Address'
        db.create_table(u'resume_address', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('address', self.gf('django.db.models.fields.TextField')(max_length=254, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('state', self.gf('django_localflavor_us.models.USStateField')(max_length=2, blank=True)),
            ('zip_code', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
        ))
        db.send_create_signal(u'resume', ['Address'])

        # Adding model 'About'
        db.create_table(u'resume_about', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('summary', self.gf('django.db.models.fields.TextField')(max_length=254)),
            ('content', self.gf('django.db.models.fields.TextField')(max_length=4096)),
        ))
        db.send_create_signal(u'resume', ['About'])

        # Adding model 'Skill'
        db.create_table(u'resume_skill', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=48)),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=254)),
        ))
        db.send_create_signal(u'resume', ['Skill'])

        # Adding model 'Experience'
        db.create_table(u'resume_experience', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('company', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('started', self.gf('django.db.models.fields.DateField')(null=True)),
            ('ended', self.gf('django.db.models.fields.DateField')(null=True)),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=2048, blank=True)),
        ))
        db.send_create_signal(u'resume', ['Experience'])

        # Adding model 'Education'
        db.create_table(u'resume_education', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('school', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('degree', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('in_progress', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('graduated', self.gf('django.db.models.fields.DateField')(null=True)),
        ))
        db.send_create_signal(u'resume', ['Education'])

        # Adding model 'Course'
        db.create_table(u'resume_course', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('course', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=254)),
        ))
        db.send_create_signal(u'resume', ['Course'])

        # Adding model 'Recommendation'
        db.create_table(u'resume_recommendation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('author', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('quote', self.gf('django.db.models.fields.TextField')(max_length=4096)),
        ))
        db.send_create_signal(u'resume', ['Recommendation'])


    def backwards(self, orm):
        # Deleting model 'Contact'
        db.delete_table(u'resume_contact')

        # Deleting model 'Address'
        db.delete_table(u'resume_address')

        # Deleting model 'About'
        db.delete_table(u'resume_about')

        # Deleting model 'Skill'
        db.delete_table(u'resume_skill')

        # Deleting model 'Experience'
        db.delete_table(u'resume_experience')

        # Deleting model 'Education'
        db.delete_table(u'resume_education')

        # Deleting model 'Course'
        db.delete_table(u'resume_course')

        # Deleting model 'Recommendation'
        db.delete_table(u'resume_recommendation')


    models = {
        u'resume.about': {
            'Meta': {'object_name': 'About'},
            'content': ('django.db.models.fields.TextField', [], {'max_length': '4096'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'summary': ('django.db.models.fields.TextField', [], {'max_length': '254'})
        },
        u'resume.address': {
            'Meta': {'object_name': 'Address'},
            'address': ('django.db.models.fields.TextField', [], {'max_length': '254', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'state': ('django_localflavor_us.models.USStateField', [], {'max_length': '2', 'blank': 'True'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'})
        },
        u'resume.contact': {
            'Meta': {'object_name': 'Contact'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone': ('django_localflavor_us.models.PhoneNumberField', [], {'max_length': '20', 'blank': 'True'})
        },
        u'resume.course': {
            'Meta': {'object_name': 'Course'},
            'course': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'resume.education': {
            'Meta': {'object_name': 'Education'},
            'degree': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'graduated': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_progress': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'school': ('django.db.models.fields.CharField', [], {'max_length': '254'})
        },
        u'resume.experience': {
            'Meta': {'object_name': 'Experience'},
            'company': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '2048', 'blank': 'True'}),
            'ended': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'started': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'type': ('django.db.models.fields.SmallIntegerField', [], {})
        },
        u'resume.recommendation': {
            'Meta': {'object_name': 'Recommendation'},
            'author': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quote': ('django.db.models.fields.TextField', [], {'max_length': '4096'})
        },
        u'resume.skill': {
            'Meta': {'object_name': 'Skill'},
            'description': ('django.db.models.fields.TextField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '48'})
        }
    }

    complete_apps = ['resume']