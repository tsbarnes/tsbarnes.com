from django.contrib import admin
from . import models

admin.site.register(models.EmailAddress)
admin.site.register(models.PhoneNumber)
admin.site.register(models.Address)
admin.site.register(models.About)
admin.site.register(models.Skill)
admin.site.register(models.Experience)
admin.site.register(models.Education)
admin.site.register(models.Course)
admin.site.register(models.Recommendation)
