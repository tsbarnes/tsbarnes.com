from django.conf.urls import patterns, url

urlpatterns = patterns('resume.views',
    url(r'^$', 'resume', name='resume'),
)
