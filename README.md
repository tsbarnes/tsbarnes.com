tsbarnes.com
============

My personal website, using:

* Django
* PIL
* South
* django-compressor
	* six
	* django-appconf
* bootstrap_admin
* django-localflavor-us
* django-taggit
* firepy
