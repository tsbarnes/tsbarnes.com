from django.template import RequestContext
from django.shortcuts import render_to_response
from . import models

def portfolio(request):
    categories = dict()
    for category in models.PortfolioCategory.objects.all():
        entries = dict()
        for entry in models.PortfolioEntry.objects.filter(category=category):
            entries[entry.slug] = dict(description=entry.description, img=entry.img, url=entry.url)
        categories[category.slug] = dict(name=category.name, entries=entries)
    context_data = dict(hide_right_sidebar=True, categories=categories)
    return render_to_response('portfolio.html', context_data, context_instance=RequestContext(request))
