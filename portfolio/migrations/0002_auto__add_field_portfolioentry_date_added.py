# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'PortfolioEntry.date_added'
        db.add_column(u'portfolio_portfolioentry', 'date_added',
                      self.gf('django.db.models.fields.DateField')(auto_now_add=True, default=datetime.datetime.now(), blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'PortfolioEntry.date_added'
        db.delete_column(u'portfolio_portfolioentry', 'date_added')


    models = {
        u'portfolio.portfoliocategory': {
            'Meta': {'object_name': 'PortfolioCategory'},
            'description': ('django.db.models.fields.TextField', [], {'max_length': '160', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'primary_key': 'True'})
        },
        u'portfolio.portfolioentry': {
            'Meta': {'object_name': 'PortfolioEntry'},
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['portfolio.PortfolioCategory']", 'symmetrical': 'False'}),
            'date_added': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '160', 'blank': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'primary_key': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['portfolio']