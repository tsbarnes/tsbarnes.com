# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'PortfolioCategory.id'
        db.add_column(u'portfolio_portfoliocategory', u'id',
                      self.gf('django.db.models.fields.AutoField')(default=0, primary_key=True),
                      keep_default=False)


        # Changing field 'PortfolioCategory.slug'
        db.alter_column(u'portfolio_portfoliocategory', 'slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50))
        # Adding field 'PortfolioEntry.id'
        db.add_column(u'portfolio_portfolioentry', u'id',
                      self.gf('django.db.models.fields.AutoField')(default=0, primary_key=True),
                      keep_default=False)


        # Changing field 'PortfolioEntry.slug'
        db.alter_column(u'portfolio_portfolioentry', 'slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50))

    def backwards(self, orm):
        # Deleting field 'PortfolioCategory.id'
        db.delete_column(u'portfolio_portfoliocategory', u'id')


        # Changing field 'PortfolioCategory.slug'
        db.alter_column(u'portfolio_portfoliocategory', 'slug', self.gf('django.db.models.fields.SlugField')(max_length=50, primary_key=True))
        # Deleting field 'PortfolioEntry.id'
        db.delete_column(u'portfolio_portfolioentry', u'id')


        # Changing field 'PortfolioEntry.slug'
        db.alter_column(u'portfolio_portfolioentry', 'slug', self.gf('django.db.models.fields.SlugField')(max_length=50, primary_key=True))

    models = {
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'portfolio.portfoliocategory': {
            'Meta': {'object_name': 'PortfolioCategory'},
            'description': ('django.db.models.fields.TextField', [], {'max_length': '160', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        u'portfolio.portfolioentry': {
            'Meta': {'object_name': 'PortfolioEntry'},
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['portfolio.PortfolioCategory']", 'symmetrical': 'False'}),
            'date_added': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '160', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'taggit.tag': {
            'Meta': {'object_name': 'Tag'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100'})
        },
        u'taggit.taggeditem': {
            'Meta': {'object_name': 'TaggedItem'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'taggit_taggeditem_tagged_items'", 'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'tag': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'taggit_taggeditem_items'", 'to': u"orm['taggit.Tag']"})
        }
    }

    complete_apps = ['portfolio']