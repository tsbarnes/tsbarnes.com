# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PortfolioCategory'
        db.create_table(u'portfolio_portfoliocategory', (
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50, primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=160, blank=True)),
        ))
        db.send_create_signal(u'portfolio', ['PortfolioCategory'])

        # Adding model 'PortfolioEntry'
        db.create_table(u'portfolio_portfolioentry', (
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50, primary_key=True)),
            ('img', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=160, blank=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200)),
        ))
        db.send_create_signal(u'portfolio', ['PortfolioEntry'])

        # Adding M2M table for field category on 'PortfolioEntry'
        db.create_table(u'portfolio_portfolioentry_category', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('portfolioentry', models.ForeignKey(orm[u'portfolio.portfolioentry'], null=False)),
            ('portfoliocategory', models.ForeignKey(orm[u'portfolio.portfoliocategory'], null=False))
        ))
        db.create_unique(u'portfolio_portfolioentry_category', ['portfolioentry_id', 'portfoliocategory_id'])


    def backwards(self, orm):
        # Deleting model 'PortfolioCategory'
        db.delete_table(u'portfolio_portfoliocategory')

        # Deleting model 'PortfolioEntry'
        db.delete_table(u'portfolio_portfolioentry')

        # Removing M2M table for field category on 'PortfolioEntry'
        db.delete_table('portfolio_portfolioentry_category')


    models = {
        u'portfolio.portfoliocategory': {
            'Meta': {'object_name': 'PortfolioCategory'},
            'description': ('django.db.models.fields.TextField', [], {'max_length': '160', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'primary_key': 'True'})
        },
        u'portfolio.portfolioentry': {
            'Meta': {'object_name': 'PortfolioEntry'},
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['portfolio.PortfolioCategory']", 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '160', 'blank': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'primary_key': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['portfolio']