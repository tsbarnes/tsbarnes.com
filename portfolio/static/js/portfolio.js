// Portfolio.js

$(window).on("load", function(event) {
    $(".gallery ul li a img").unwrap();
});

$(".gallery ul li").on("click", function(event) {
    var src = $("img", this).attr("src");
    var href = $("img", this).attr("data-link");
    console.log(href);
    $(".preview img").attr("src", src);
    $(".preview figcaption a").attr("href", href);
    $(".preview").fadeIn();
});

$(".preview .close").on("click", function(event) {
    $(".preview").fadeOut();
});
