from django.db import models
from taggit.managers import TaggableManager

class PortfolioCategory(models.Model):
    class Meta:
        verbose_name_plural = 'Portfolio categories'
    slug = models.SlugField(max_length=50, unique=True)
    name = models.CharField(max_length=50, blank=False)
    description = models.TextField(max_length=160, blank=True)
    
    def __unicode__(self):
        return "Portfolio Category: " + self.slug

class PortfolioEntry(models.Model):
    class Meta:
        verbose_name_plural = 'Portfolio entries'
    slug = models.SlugField(max_length=50, unique=True)
    img = models.ImageField(upload_to='portfolio')
    description = models.TextField(max_length=160, blank=True)
    url = models.URLField(max_length=200, blank=False)
    category = models.ForeignKey(PortfolioCategory)
    date_added = models.DateField(auto_now_add=True)
    tags = TaggableManager(blank=True)
    
    def __unicode__(self):
        return "Portfolio Entry: " + self.slug