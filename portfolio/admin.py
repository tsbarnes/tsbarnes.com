from django.contrib import admin
from . import models

class PortfolioEntryAdmin(admin.ModelAdmin):
    readonly_fields = ('date_added',)
    list_display = ('slug', 'description')

admin.site.register(models.PortfolioCategory)
admin.site.register(models.PortfolioEntry, PortfolioEntryAdmin)
