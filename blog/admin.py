from django.contrib import admin
from . import models

class BlogEntryAdmin(admin.ModelAdmin):
    readonly_fields = ('posted', 'edited')
    list_display = ('title', 'slug', 'posted', 'edited')

admin.site.register(models.BlogCategory)
admin.site.register(models.BlogEntry, BlogEntryAdmin)
