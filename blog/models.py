from django.db import models
from taggit.managers import TaggableManager

class BlogCategory(models.Model):
    class Meta:
        verbose_name_plural = 'Blog categories'
    slug = models.SlugField(max_length=50, unique=True)
    name = models.CharField(max_length=50)
    description = models.TextField(max_length=160, blank=True)
    
    def __unicode__(self):
        return "Blog Category: " + self.slug

class BlogEntry(models.Model):
    class Meta:
        verbose_name_plural = 'Blog entries'
    slug = models.SlugField(max_length=50, unique=True)
    title = models.CharField(max_length=100)
    content = models.TextField(max_length=2000)
    category = models.ForeignKey(BlogCategory, null=True)
    posted = models.DateTimeField(auto_now_add=True)
    edited = models.DateTimeField(auto_now=True)
    tags = TaggableManager(blank=True)
    
    def __unicode__(self):
        return "Blog Entry: " + self.slug
