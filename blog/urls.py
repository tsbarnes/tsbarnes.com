from django.conf.urls import patterns, url

urlpatterns = patterns('blog',
    url(r'^$', 'views.index', name='blog'),
    url(r'^(?P<slug>.*)$', 'views.entry', name='blog-entry'),
)
