from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import Http404
from . import models

def index(request):
    context_data = dict(hide_right_sidebar=True, blog=models.BlogEntry.objects.order_by('posted').all()[:10])
    return render_to_response('blog.html', context_data, context_instance=RequestContext(request))

def entry(request, slug):
    try:
        selected = models.BlogEntry.objects.get(slug=slug)
    except models.BlogEntry.DoesNotExist:
        raise Http404
    return render_to_response('blog_entry.html', { 'entry': selected })
